//
//  ThemeSingleton.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 26/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import Foundation

class ThemeContainer {
    
    final let themeKey = "ThemeIndex"
    
    static let sharedInstance = ThemeContainer()
        
    var current: ThemeType {
        get {
            guard let themeIndex = UserDefaults.standard.value(forKey: themeKey) as? Int else {
                return .pink
            }
            return ThemeType.allValues[themeIndex]
        }
        set {
            UserDefaults.standard.set(ThemeType.allValues.index(of: newValue), forKey: themeKey)
        }
    }
}
