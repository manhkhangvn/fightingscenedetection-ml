//
//  ButtonClean.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 27/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

class ButtonClean: UIButton, CleanTheme {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buttonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buttonSetup() {
        self.setTitleColor(.white, for: .normal)
        self.layer.cornerRadius = 2.0
    }
}
