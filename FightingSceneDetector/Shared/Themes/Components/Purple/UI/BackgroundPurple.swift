//
//  BackgroundPurple.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 26/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

class BackgroundPurple: PurpleTheme {
    final let mainThemeColor: ColorPalette = ThemeType.purple.getColorsPalette()
    
    func makeGradientColors() -> (ThemeColors, ThemeColorPosition) {
        return (
            ThemeColors(colors: [
                mainThemeColor.colours.primary.withAlphaComponent(0.7),
                mainThemeColor.colours.secondary.withAlphaComponent(0.5),
                mainThemeColor.colours.bonus.withAlphaComponent(0.3)
                ]),
            ThemeColorPosition(positions: [0.6, 0.8, 0.9])
        )
    }
}
