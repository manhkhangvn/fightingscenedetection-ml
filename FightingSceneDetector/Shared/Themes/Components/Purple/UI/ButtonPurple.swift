//
//  ButtonPurple.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 26/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

class ButtonPurple: UIButton, PurpleTheme {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buttonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buttonSetup() {
        self.setTitleColor(.white, for: .normal)
        self.layer.cornerRadius = 7.0
    }
}
