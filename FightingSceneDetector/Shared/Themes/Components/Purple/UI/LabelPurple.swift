//
//  LabelPurple.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 26/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

class LabelPurple: UILabel, PurpleTheme {
    override init(frame: CGRect) {
        super.init(frame: frame)
        labelSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func apply(toView view: UIView) {
        view.addSubview(self)
    }
    
    private func labelSetup() {
        self.textColor = .white
    }
}
