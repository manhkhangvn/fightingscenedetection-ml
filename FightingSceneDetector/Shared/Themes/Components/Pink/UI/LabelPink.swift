//
//  LabelPink.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 23/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

class LabelPink: UILabel, PinkTheme {
    override init(frame: CGRect) {
        super.init(frame: frame)
        labelSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func apply(toView view: UIView) {
        view.addSubview(self)
    }
    
    private func labelSetup() {
        self.textColor = .white
    }
}
