//
//  Color.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 26/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

struct Colours {
    var primary, secondary, bonus: UIColor
    
    init(colors: [UIColor]) {
        (primary, secondary, bonus) = (colors[0], colors[1], colors[2])
    }
}

struct ColorPalette {
    var colours: Colours
    
    init(colors: [UIColor]) {
        self.colours = Colours(colors: colors)
    }
}

enum ThemeType: String {
    case pink, yellow, purple, energeticClean = "Energetic Clean"
    
    func getBackgroundGradient(masterFactory: MasterThemeFactory) -> (ThemeColors, ThemeColorPosition)? {
        return masterFactory.backgroundFactory.makeGradientColors()
    }
    
    func getColorsPalette() -> ColorPalette {
        switch self {
        case .purple:
            return ColorPalette(colors: [UIColor(red:0.56, green:0.06, blue:0.64, alpha:1.0),
                                         UIColor(red:0.85, green:0.06, blue:0.36, alpha:1.0),
                                         UIColor(red:0.31, green:0.11, blue:0.67, alpha:1.0)])
        case .yellow:
            return ColorPalette(colors: [UIColor(red:1.00, green:0.69, blue:0.00, alpha:1.0),
                                         UIColor(red:1.00, green:0.85, blue:0.00, alpha:1.0),
                                         UIColor(red:1.00, green:0.50, blue:0.00, alpha:1.0)])
        case .pink:
            return ColorPalette(colors: [UIColor(red:0.88, green:0.00, blue:0.48, alpha:1.0),
                                         UIColor(red:1.00, green:0.03, blue:0.00, alpha:1.0),
                                         UIColor(red:0.53, green:0.03, blue:0.78, alpha:1.0)])
        case .energeticClean:
            return ColorPalette(colors: [UIColor(red:0.34, green:0.50, blue:0.91, alpha:1.0),
                                         UIColor(red:0.52, green:0.81, blue:0.92, alpha:1.0),
                                         UIColor(red:0.53, green:0.38, blue:0.82, alpha:1.0)])
        }
    }
    
    static let allValues = [pink, yellow, purple, energeticClean]
}
