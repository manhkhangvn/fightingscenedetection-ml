//
//  ThemeFactory.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 23/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

protocol ThemeFactory {
    var currentTheme: ThemeType { get set }
    
    init(theme: ThemeType)
    
    func makeTheme() -> Theme
}
