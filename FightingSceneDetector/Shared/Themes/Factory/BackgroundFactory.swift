//
//  BackgroundFactory.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 23/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

class BackgroundFactory: ThemeFactory {
    
    var currentTheme: ThemeType
    
    required init(theme: ThemeType) {
        currentTheme = theme
    }
    
    func makeTheme() -> Theme {        
        switch currentTheme {
        case .pink:
            return BackgroundPink()
            
        case .yellow:
            return BackgroundYellow()
            
        case .purple:
            return BackgroundPurple()
            
        case .energeticClean:
            return BackgroundClean()
        }
    }
    
    func makeGradientColors() -> (ThemeColors, ThemeColorPosition) {
        switch currentTheme {
        case .pink:
            return BackgroundPink().makeGradientColors()
            
        case .yellow:
            return BackgroundYellow().makeGradientColors()
            
        case .purple:
            return BackgroundPurple().makeGradientColors()
            
        case .energeticClean:
            return BackgroundClean().makeGradientColors()
        }
    }
}
