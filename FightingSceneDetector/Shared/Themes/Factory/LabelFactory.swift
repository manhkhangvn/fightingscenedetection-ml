//
//  LabelFactory.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 23/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

class LabelFactory: ThemeFactory {

    var currentTheme: ThemeType
    
    required init(theme: ThemeType) {
        currentTheme = theme
    }
    
    func makeTheme() -> Theme {        
        switch currentTheme {
        case .pink:
            return LabelPink()
        
        case .yellow:
            return LabelYellow()
        
        case .purple:
            return LabelPurple()
            
        case .energeticClean:
            return LabelClean()
        }
    }
}
