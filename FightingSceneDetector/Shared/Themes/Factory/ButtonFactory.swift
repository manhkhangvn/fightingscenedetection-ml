//
//  ButtonFactory.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 23/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

class ButtonFactory: ThemeFactory {
    
    var currentTheme: ThemeType
    
    required init(theme: ThemeType) {
        currentTheme = theme
    }
    
    func makeTheme() -> Theme {
        switch currentTheme {
        case .pink:
            return ButtonPink()
            
        case .yellow:
            return ButtonYellow()
            
        case .purple:
            return ButtonPurple()
            
        case .energeticClean:
            return ButtonClean()
        }
    }
    
    func createButton() -> UIButton {
        switch currentTheme {
        case .pink:
            return ButtonPink()
            
        case .yellow:
            return ButtonYellow()
            
        case .purple:
            return ButtonPurple()
            
        case .energeticClean:
            return ButtonClean()
        }
    }
}
