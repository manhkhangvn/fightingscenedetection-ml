//
//  MasterFactory.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 23/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

struct ThemeColors {
    var colors: [UIColor]!
    
    init(colors: [UIColor]) {
        self.colors = colors
    }
}

struct ThemeColorPosition {
    var positions: [Double]!
    
    init(positions: [Double]) {
        self.positions = positions
    }
}

class MasterThemeFactory {
    
    var currentTheme: ThemeType = ThemeContainer.sharedInstance.current
    
    lazy var buttonFactory: ButtonFactory = ButtonFactory(theme: currentTheme)
    lazy var labelFactory: LabelFactory = LabelFactory(theme: currentTheme)
    lazy var backgroundFactory: BackgroundFactory = BackgroundFactory(theme: currentTheme)
    
    func updateCurrentTheme(theme: ThemeType) {
        buttonFactory.currentTheme = theme
        labelFactory.currentTheme = theme
        backgroundFactory.currentTheme = theme
    }
}
