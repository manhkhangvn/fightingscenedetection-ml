//
//  ThemeControl.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 26/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import Foundation

protocol ThemeControl {
    var masterThemeFactory: MasterThemeFactory { get }
    
    func updateTheme(withTheme type: ThemeType)
}
