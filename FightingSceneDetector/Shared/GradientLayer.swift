//
//  GradientLayer.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 28/2/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit

public final class GradientLayer: CAGradientLayer {
    
    public init(bounds: CGRect, colors: [UIColor]) {
        super.init()
        
        self.frame = bounds
        self.colors = colors.map { $0.cgColor }
    }
    
    public func setLocation(locations: [Double]) {
        self.locations = locations.map { NSNumber(value: $0) }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
