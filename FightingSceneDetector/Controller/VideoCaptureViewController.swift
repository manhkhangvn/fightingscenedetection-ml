//
//  VideoCaptureViewController.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 1/3/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit
import AVKit
import Vision

class VideoCaptureViewController: UIViewController {
    
    private lazy var captureSession: AVCaptureSession = {
        let session = AVCaptureSession()
        session.sessionPreset = .photo
        return session
    }()
    
    private enum Metric {
        static let mainScreen: CGRect = UIScreen.main.bounds
        static let padding: CGFloat = 20.0
        static let commonFontSize: CGFloat = 20.0
        static let commonButtonSize: CGSize = CGSize(width: 120.0, height: 30.0)
        static let buttonSize: CGSize = CGSize(width: mainScreen.size.width - padding * 2.0,
                                               height: 30.0)
    }
    
    var startCaptureButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private var resultLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Proccessing..."
        label.textColor = .white
        label.layer.cornerRadius = 16.0
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    lazy var masterThemeFactory: MasterThemeFactory = { return MasterThemeFactory() }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = masterThemeFactory.currentTheme.getColorsPalette()
            .colours.primary.withAlphaComponent(0.3)
        navigationItem.title = "Live detection"
        
        layout()
        bind()
    }
    
    func layout() {
        [startCaptureButton, resultLabel].forEach(self.view.addSubview)
        
        NSLayoutConstraint.activate([
            startCaptureButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            startCaptureButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            startCaptureButton.widthAnchor.constraint(equalToConstant: Metric.buttonSize.width),
            startCaptureButton.heightAnchor.constraint(equalToConstant: Metric.buttonSize.height),
            
            resultLabel.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,
                                                constant: -Metric.padding),
            resultLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            resultLabel.widthAnchor.constraint(equalToConstant: Metric.buttonSize.width)
        ])
    }
    
    func bind() {
        setupCamera()
    }
    
    // MARK: - Helpers
    
    func setupCamera() {
        guard let captureDevice = AVCaptureDevice.default(for: .video) else { return }
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(input)
            
            captureSession.startRunning()
        } catch {
            debugPrint(error.localizedDescription)
        }
        
        /// Preview Layer
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = self.view.bounds
        self.view.layer.addSublayer(previewLayer)
        
        visionSetup()
    }
    
    func visionSetup() {
        /// Data Ouput will monitor the frames every time the camera captures
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: .userInitiated))
        captureSession.addOutput(dataOutput)
    }
    
    @objc func dismissVC(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension VideoCaptureViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput,
                       didOutput sampleBuffer: CMSampleBuffer,
                       from connection: AVCaptureConnection) {
        
        guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        guard let fightingModel = try? VNCoreMLModel(for: fightVsNonFight().model) else {
            debugPrint("Error: Can't load ML Model!")
            return
        }
        
        let request = VNCoreMLRequest(model: fightingModel) { (request, error) in
            guard
                let results = request.results as? [VNClassificationObservation],
                let topResult = results.first
            else { return }
            
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                debugPrint(topResult.identifier)
                
                let fightingText = NSAttributedString(string: "is", attributes: [
                    .font: UIFont.boldSystemFont(ofSize: Metric.commonFontSize)
                    ])
                let nonFightingText = NSAttributedString(string: "is NOT", attributes: [
                    .font: UIFont.boldSystemFont(ofSize: Metric.commonFontSize)
                    ])
                let result = NSMutableAttributedString(string: "This image ")
                result.append((topResult.identifier == "fight") ? fightingText : nonFightingText)
                result.append(NSAttributedString(string: " a fighting scene\nConfidence: "))
                result.append(NSAttributedString(string: "\(Int(topResult.confidence * 100))%", attributes: [
                    .font: UIFont.boldSystemFont(ofSize: Metric.commonFontSize),
                    .foregroundColor: self.masterThemeFactory.currentTheme.getColorsPalette().colours.primary
                ]))
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.resultLabel.attributedText = result
                })
            }
        }
        
        do { // Perform request handler for predictions
            try VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:])
                .perform([request])
        } catch {
            debugPrint(error.localizedDescription)
        }
    }
}


// MARK: - Theme control

extension VideoCaptureViewController: ThemeControl {
    
    func updateTheme(withTheme type: ThemeType) {
        // More here...
    }
}
