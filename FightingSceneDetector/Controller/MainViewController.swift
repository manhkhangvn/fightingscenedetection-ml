//
//  MainViewController.swift
//  FightingSceneDetector
//
//  Created by Khang Vu on 28/2/18.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import UIKit
import CoreML
import Vision
import Dropdowns

class MainViewController: UIViewController {
    
    // MARK: - Variables and Metrics
    
    private final let testImages = [#imageLiteral(resourceName: "images_1"), #imageLiteral(resourceName: "images_2"), #imageLiteral(resourceName: "images_3"), #imageLiteral(resourceName: "images_4"), #imageLiteral(resourceName: "images_5"), #imageLiteral(resourceName: "images_6"), #imageLiteral(resourceName: "images_7"), #imageLiteral(resourceName: "images_8"), #imageLiteral(resourceName: "images_9"), #imageLiteral(resourceName: "images_11"),
                                    #imageLiteral(resourceName: "images_13"), #imageLiteral(resourceName: "images_12"), #imageLiteral(resourceName: "images_14"), #imageLiteral(resourceName: "images_15"), #imageLiteral(resourceName: "images_16"), #imageLiteral(resourceName: "images_17"), #imageLiteral(resourceName: "images_18"), #imageLiteral(resourceName: "images_19"), #imageLiteral(resourceName: "images_20")]
    
    private var currentTestImageIndex = 0
    
    private struct Metric {
        static let padding: CGFloat = 20.0
        static let labelHeight: CGFloat = 50.0
        static let commonFontSize: CGFloat = 20.0
        static let commonImageSize: CGSize = CGSize(width: 30.0, height: 30.0)
        static let switchSize: CGSize = CGSize(width: 50.0, height: 30.0)
        static let mainScreen: CGRect = UIScreen.main.bounds
        static let detectImageSize: CGSize = CGSize(width: mainScreen.size.width - padding,
                                                    height: mainScreen.size.height * 1/2)
    }
    
    private var gradientLayer: GradientLayer?
    
    private lazy var detectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 16.0
        imageView.layer.borderWidth = 2.5
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.contentMode = .scaleAspectFit
        imageView.image = #imageLiteral(resourceName: "Placeholder")
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true

        return imageView
    }()
    
    private lazy var predictionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.italicSystemFont(ofSize: UIFont.systemFontSize)
        label.text = "The chosen image prediction will be shown here"
        label.sizeToFit()
        
        return label
    }()
    
    private lazy var pickImageButton: UIButton = {
        let button = masterThemeFactory.buttonFactory.createButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(takePicture(sender:)), for: .touchUpInside)
        button.sizeToFit()
        button.setTitle("    Pick image / Take a screenshot    ", for: .normal)
        
        return button
    }()
    
    private lazy var useTestImageButton: UIBarButtonItem = {
        let button = UIBarButtonItem(barButtonSystemItem: .play, target: self,
                                     action: #selector(useTestImages))
        button.tintColor = .white
        return button
    }()
    
    private lazy var videoCaptureButton: UIBarButtonItem = {
        let button = UIBarButtonItem(barButtonSystemItem: .camera, target: self,
                                     action: #selector(videoCapture))
        button.tintColor = .white
        return button
    }()
    
    private var titleView: TitleView?
    
    private var shouldUseTestImages: Bool = false
    
    lazy var masterThemeFactory: MasterThemeFactory = { return MasterThemeFactory() }()
    
    // MARK: - Main
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain,
                                                           target: nil, action: nil)
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        
        layout()
        bind()
    }
    
    // MARK: Layout
    
    private func setupNavBar() {
        navigationItem.leftBarButtonItem = useTestImageButton
        navigationItem.rightBarButtonItem = videoCaptureButton
        
        let items = ThemeType.allValues.map { "\($0.rawValue.capitalized) " }
        titleView = TitleView(navigationController: navigationController!,
                              title: "Theme ", items: items)
        
        /// Theme selection action
        titleView?.action = { [unowned self] index in
            debugPrint("Selected \(ThemeType.allValues[index].rawValue) theme.")
            let type = ThemeType.allValues[index]
            
            /// Update master Theme
            ThemeContainer.sharedInstance.current = type
            
            UIView.animate(withDuration: 0.5, animations: {
                self.masterThemeFactory.updateCurrentTheme(theme: type)
            })
            self.updateTheme(withTheme: type)
        }
        navigationItem.titleView = titleView
    }
    
    private func layout() {
        UIApplication.shared.statusBarStyle = .lightContent
        Config.ArrowButton.Text.color = .white
        Config.List.backgroundColor = UIColor.clear.withAlphaComponent(0.6)
        
        setupNavBar()
        
        [detectImageView, pickImageButton, predictionLabel]
            .forEach(self.view.addSubview)
        
        NSLayoutConstraint.activate([
            /// Image view to detect
            detectImageView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor,
                                                 constant: Metric.padding),
            detectImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            detectImageView.widthAnchor.constraint(equalToConstant: Metric.detectImageSize.width),
            detectImageView.heightAnchor.constraint(equalToConstant: Metric.detectImageSize.height),
            
            /// Prediction label - displays result
            predictionLabel.topAnchor.constraint(equalTo: detectImageView.bottomAnchor, constant: Metric.padding),
            predictionLabel.trailingAnchor.constraint(equalTo: detectImageView.trailingAnchor),
            predictionLabel.leadingAnchor.constraint(equalTo: detectImageView.leadingAnchor),
            
            /// Pick image button
            pickImageButton.topAnchor.constraint(equalTo: predictionLabel.bottomAnchor, constant: Metric.padding),
            pickImageButton.centerXAnchor.constraint(equalTo: detectImageView.centerXAnchor),
            pickImageButton.heightAnchor.constraint(equalToConstant: Metric.labelHeight)
        ])
    }
    
    private func bind() {
        updateTheme(withTheme: masterThemeFactory.currentTheme)
        shouldUseTestImages = false
    }
    
    // MARK: Photo Actions
    
    func back() {
        guard currentTestImageIndex > 0 else { return }
        currentTestImageIndex -= 1
        test(image: testImages[currentTestImageIndex])
    }
    
    func next() {
        guard currentTestImageIndex < testImages.count - 1 else { return }
        currentTestImageIndex += 1
        test(image: testImages[currentTestImageIndex])
    }
    
    @objc func useTestImages() {
        shouldUseTestImages = true
        test(image: testImages.first!)
    }
    
    @objc func videoCapture() {
        titleView?.dropdown.hide() // Hide dropdown if opened
        
        let videoCaptureVC = VideoCaptureViewController()
        navigationController?.pushViewController(videoCaptureVC, animated: true)
    }
    
    @objc func takePicture(sender: UIButton) {
        shouldUseTestImages = false
        
        // Show options for the source picker only if the camera is available.
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            presentPhotoPicker(sourceType: .photoLibrary)
            return
        }
        
        let photoSourcePicker = UIAlertController()
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { [unowned self] _ in
            self.presentPhotoPicker(sourceType: .camera)
        }
        let choosePhoto = UIAlertAction(title: "Choose Photo", style: .default) { [unowned self] _ in
            self.presentPhotoPicker(sourceType: .photoLibrary)
        }
        
        photoSourcePicker.addAction(takePhoto)
        photoSourcePicker.addAction(choosePhoto)
        photoSourcePicker.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(photoSourcePicker, animated: true)
        
        UIView.animate(withDuration: 0.5) {
            self.predictionLabel.alpha = 0.0
        }
    }
    
    func presentPhotoPicker(sourceType: UIImagePickerControllerSourceType) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = sourceType
        
        present(picker, animated: true) {
            UIApplication.shared.statusBarStyle = .default
        }
    }
    
    // MARK: Touch actions
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: detectImageView)
        print(location, self.view.center)
        
        guard shouldUseTestImages else { return }
        location.x <= self.view.center.x ? back() : next()
    }
}

// MARK: - Theme control

extension MainViewController: ThemeControl {
    
    func updateTheme(withTheme type: ThemeType) {
        /// Background gradient and button style
        applyGradient(withTheme: type)
        
        /// Navigation bar color
        self.navigationController?.navigationBar.barTintColor = type.getColorsPalette().colours.primary
        
        /// Prediction label color
        predictionLabel.textColor = .white
        
        /// Pick image button
        pickImageButton.backgroundColor = type.getColorsPalette().colours.secondary
    }
    
    // MARK: Theme local custom methods
    
    private func applyGradient(withTheme type: ThemeType) {
        if let layer = gradientLayer { layer.removeFromSuperlayer() }
        guard let background = type.getBackgroundGradient(masterFactory: masterThemeFactory) else { return }
        
        self.gradientLayer = GradientLayer(bounds: self.view.bounds, colors: background.0.colors)
        self.gradientLayer!.setLocation(locations: background.1.positions)
        
        self.view.layer.addSublayer(self.gradientLayer!)
        
        self.view.subviews.forEach { subview in self.view.bringSubview(toFront: subview) }
    }
}

// MARK: - Machine Learning

extension MainViewController {
    
    func detectScene(image: CIImage, orientation: CGImagePropertyOrientation? = nil) {
        predictionLabel.text = "Detecting scene..."
        
        DispatchQueue.global(qos: .userInitiated).async {
            var handler: VNImageRequestHandler?
            if let orientation = orientation {
                handler = VNImageRequestHandler(ciImage: image, orientation: orientation)
            } else {
                handler = VNImageRequestHandler(ciImage: image)
            }
            do {
                guard let request = self.classificationRequest() else { return }
                try handler!.perform([request])
            } catch {
                debugPrint("Failed to perform classification.\n\n\(error.localizedDescription)")
            }
        }
    }
    
    private func classificationRequest() -> VNCoreMLRequest? {
        
        guard let fightingModel = try? VNCoreMLModel(for: fightVsNonFight().model) else {
            debugPrint("Error: Can't load ML Model!")
            return nil
        }
        
        let request = VNCoreMLRequest(model: fightingModel, completionHandler: { [unowned self] (request, error) in
            self.processClassifications(for: request, error: error)
        })
        request.imageCropAndScaleOption = .centerCrop
        return request
    }
    
    private func processClassifications(for request: VNRequest, error: Error?) {
        
        
        guard let results = request.results as? [VNClassificationObservation],
            let topResult = results.first
        else {
            debugPrint("Unexpected result from CoreML model")
            return
        }
        
        /// Update on Main queue
        DispatchQueue.main.async {
            let fightingText = NSAttributedString(string: "is", attributes: [
                .font: UIFont.boldSystemFont(ofSize: Metric.commonFontSize)
            ])
            let nonFightingText = NSAttributedString(string: "is NOT", attributes: [
                .font: UIFont.boldSystemFont(ofSize: Metric.commonFontSize)
            ])
            let result = NSMutableAttributedString(string: "This image ")
            result.append((topResult.identifier == "fight") ? fightingText : nonFightingText)
            result.append(NSAttributedString(string: " a fighting scene, "))
            result.append(NSAttributedString(string: "confidence: \(Int(topResult.confidence * 100))%", attributes: [
                .font: UIFont.boldSystemFont(ofSize: Metric.commonFontSize)
            ]))
            
            debugPrint(topResult.identifier)
            UIView.animate(withDuration: 0.5, animations: {
                self.predictionLabel.attributedText = result
                self.predictionLabel.alpha = 1.0
            })
        }
    }
}

// MARK: - Handling Image Picker Selection

extension MainViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        self.detectImageView.contentMode = .scaleAspectFit
        picker.dismiss(animated: true) {
            UIApplication.shared.statusBarStyle = .lightContent
        }
        
        // We always expect this method to supply the original image.
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            debugPrint("Unavailable original image")
            return
        }
        test(image: image)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            UIApplication.shared.statusBarStyle = .lightContent
        }
    }
    
    func test(image: UIImage) {
        detectImageView.image = image
        
        guard let ciImage = CIImage(image: image),
            let orientation = CGImagePropertyOrientation(rawValue: UInt32(image.imageOrientation.rawValue))
        else { fatalError("Unable to create \(CIImage.self) from \(image).") }
        
        detectScene(image: ciImage, orientation: orientation)
    }
}
