import turicreate as tc

## Tag images - Make sure the names are independent to each other!
data = tc.image_analysis.load_images('../images', with_path = True)
data['label'] = data['path'].apply(lambda path: 'fight' if 'fight' in path else 'nonFight')

print(data.groupby('label', [tc.aggregate.COUNT]))

## Save data and visualize the sFrame for two categories
data.save('fightVsNonFight.sframe')
data.explore()
