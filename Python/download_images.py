# this script is used to download the images using the provided url
import requests
import ntpath
import os

# the file which contain the images
image_urls_file_name = '../data/non-fight-cars.synset.txt'

# the destination of the images inside the images folder
destination_folder = '../images/nonFight_cars/'

# Create file if not existed
if not os.path.exists(os.path.dirname(destination_folder)):
    try:
        os.makedirs(os.path.dirname(destination_folder))
    except:
        if exc.errno != errno.EEXIST:
            raise

# save image data
def save_image_data(image_data, file_name):
    with open(destination_folder + file_name, 'wb') as file_object:
        file_object.write(image_data)

# read the images_url file
with open(image_urls_file_name) as file_object:

    print("------DOWNLOADING AND SAVING THE IMAGES---------")
    image_index = 0
    failed_images = 0

    for line in file_object:
        image_index += 1 # Keep track of number of downloaded images
        file_name = ntpath.basename(line.strip())
        
        try:
            image_data = requests.get(line).content
        except:
            failed_images += 1
            print("error download an image")

        # save the image
        print('%s || %d downloaded, %d failed' % (file_name, image_index, failed_images))

        save_image_data(image_data,file_name)
