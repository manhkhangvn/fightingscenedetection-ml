import turicreate as tc

modelName = 'fightVsNonFight.model'
coremlName = 'fightVsNonFight.mlmodel'

## Load pre-proccessed data
data = tc.SFrame('fightVsNonFight.sframe')

training_data, validation_data = data.random_split(0.8)


## Create training model
model = tc.image_classifier.create(
	training_data,
	target = 'label',
	max_iterations = 100,
	verbose = True
)


## Make predictions
print('--- Normal Prediction ---')
predictions = model.predict(validation_data)
predictions

print('--- Prediction with confidence ---')
predictions = model.classify(validation_data)
predictions

## Evaluate the model with ground truth data by getting accuracy for predictions
metrics = model.evaluate(validation_data)

print('---Accuracy---')
print(metrics['accuracy'])


## Save and export newly trained model
model.save(modelName)
model.export_coreml(coremlName)

print('---Model saved and exported to CoreML---')
